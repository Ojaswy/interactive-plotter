document.getElementById('plotButton').addEventListener('click', function() {
    const A = parseFloat(document.getElementById('paramA').value);
    const B = parseFloat(document.getElementById('paramB').value);
    const C = parseFloat(document.getElementById('paramC').value);
    const K = parseFloat(document.getElementById('paramK').value);

    const rangeX = document.getElementById('rangeX').value.split(':').map(Number);
  
    const xValues = [];
    const yValues = [];
    const step = (rangeX[1] - rangeX[0]) / 100;
  
    for (let x = rangeX[0]; x <= rangeX[1]; x += step) {
      xValues.push(x);
      yValues.push(Math.exp(-A * x) * Math.sin(B *K * Math.pow(x, C)));
    }
  
    const trace = {
      x: xValues,
      y: yValues,
      type: 'scatter',
      mode: 'lines',
      name: 'Function Plot'
    };
  
    const layout = {
      title: 'Function Plot',
      xaxis: {
        title: 'x'
      },
      yaxis: {
        title: 'y'
      }
    };
  
    Plotly.newPlot('plot', [trace], layout);
  });
  